function createLocation(locationName, locationID) {
    return `
    <option value=${locationID}>${locationName}</option>
    `
};
  

window.addEventListener("DOMContentLoaded", async () => {
    const url = "http://localhost:8000/api/locations/";

    try {
        const response = await fetch(url);

        const elementsToRender = [];

        if (!response.ok) {
            throw new Error('Response not ok');
        } else {
            const data = await response.json();

            const selectTag = document.getElementById("location")
            for (let location of data.locations) {
                const locationName = location.name;
                const locationId = location.id;
                const option = createLocation(locationName, locationId);
                elementsToRender.push(option);
            }
            selectTag.innerHTML = elementsToRender.join("");

            const formTag = document.getElementById("create-conference-form");
            formTag.addEventListener("submit", async (event) => {
                event.preventDefault();

                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData))

                const conferenceUrl = "http://localhost:8000/api/conferences/";
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        "Content-Type": "application/json",
                    },
                };
                const response = await fetch(conferenceUrl, fetchConfig);

                if (response.ok) {
                    formTag.reset();
                    const newConference = await response.json();
                };
            })
        }
    } catch (error) {
        console.error('error', error);
    }

});