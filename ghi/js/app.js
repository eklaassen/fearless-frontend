function createCard(name, description, pictureUrl, startDateString, endDateString, locationName) {
    return `
    <div class="card shadow-lg">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${locationName}</h6>
            <p class="card-text">${description}</p>
        </div>
        <div class="card-footer text-muted">
            ${startDateString} - ${endDateString}
        </div>
    </div>
    `;
}

function createPlaceholder() {
    return `
    <div class="card shadow mb-5 bg-white rounded" style="height: min-content">
        <div class="card-img-top placeholder-glow" style="width: 100%; height: 200px">
            <div class="placeholder" style="height: 100%; width: 100%"></div>
        </div>
        <div class="card-body">
            <h5 class="card-title placeholder-glow">
            <span class="placeholder col-6"></span>
            </h5>
            <h6 class="card-subtitle mb-2 text-muted placeholder-glow">
            <span class="placeholder col-6"></span>
            </h6>
            <p class="card-text placeholder-glow">
            <span class="placeholder col-7"></span>
            <span class="placeholder col-4"></span>
            <span class="placeholder col-4"></span>
            <span class="placeholder col-6"></span>
            <span class="placeholder col-8"></span>
            </p>
        </div>
        <div class="card-footer placeholder-glow">
            <span class="placeholder col-7"></span>
        </div>
    </div>
    `
}

function generateError(message) {
    return `
    <div class="alert alert-primary" role="alert">
        We are so sorry, something went wrong! Please try again later.
        ${message}
    </div>
    `;
}

window.addEventListener('DOMContentLoaded', async () => {
    const url = 'http://localhost:8000/api/conferences/';

    try {
        // takes an array and turns it into a string to render into the innerHTML
        function updateRowDOM(row, elementsToRender) {
            row.innerHTML = elementsToRender.join("");
        }
  
        // declared at top level because used multiple times
        const row = document.querySelector(".row");
        // one placeholder card already exists in the html so we declare it here
        // created an array to keep track of the current list of elements (placeholder/card) to render in the row element
        const elementsToRender = [];

        const response = await fetch(url);
        if (!response.ok) {
            throw new Error('Response not ok');
        } else {
            const data = await response.json();
            for (let conference of data.conferences) {
                // create a placeholder card for the current conference
                elementsToRender.push(createPlaceholder());
                // updates every time a change is made
                updateRowDOM(row, elementsToRender);


                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const pictureUrl = details.conference.location.picture_url;

                    // Get conference dates
                    const startDate = new Date(details.conference.starts);
                    const startDateString = startDate.toLocaleDateString();
                    const endDate = new Date(details.conference.ends);
                    const endDateString = endDate.toLocaleDateString();

                    // Get location name
                    const locationName = details.conference.location.name;
                    const html = createCard(title, description, pictureUrl, startDateString, endDateString, locationName);

                    elementsToRender.pop(); // remove the current placeholder card
                    elementsToRender.push(html); // replace it with real card
                    updateRowDOM(row, elementsToRender); // update

                    // const column = document.querySelector('.row');
                    // column.innerHTML += html;
                }
            }
        }
    } catch (error) {
        console.error("error", error);
        const html = generateError(error.message);
        const row = document.querySelector('.row');
        row.innerHTML += html;
    }
  
  });