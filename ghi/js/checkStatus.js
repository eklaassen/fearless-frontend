const payloadCookie = await cookieStore.get('jwt_access_payload');

if (payloadCookie) {
    // Convert the encoded payload from base64 to normal string
    const decodedPayload = atob(payloadCookie.value);

    // The payload is a JSON-formatted string, so parse it
    const payload = JSON.parse(decodedPayload);
    console.log(payload);

    if (payload.user.perms.includes("events.add_conference")) {
        const conferenceLinkTag = document.getElementById("newConferenceNavLink");
        conferenceLinkTag.classList.remove("d-none");
    }

    if (payload.user.perms.includes("events.add_location")) {
        const locationLinkTag = document.getElementById("newLocationNavLink");
        locationLinkTag.classList.remove("d-none");
    }

    if(payload.user.perms.includes("presentations.add_presentation")) {
        const presentationLinkTag = document.getElementById("newPresentationNavLink");
        presentationLinkTag.classList.remove("d-none");
    }

} else {
    console.log("Cookie not found");
}